## I recommend running this script using the folder in which the
## script is contained as the working directory, so that the user
## credentials and the log folder (if you enable that) are all in the
## same place
from mastodon import Mastodon

import os

mastodon = Mastodon(
    access_token = 'pytooter_usercred.secret',
    api_base_url = 'https://scholar.social' # This will need to be
                                            # updated with the correct
                                            # instance
)

# The code below is ~ the same, but doubled, here it's done for the
# word "introduction" and below it's the same stuff but for
# "introductions"; if I wanted to do it properly I'd write a function
# or something but I didn't, sorry

## Get the ID of the last post that was checked
with open("introduction_last_id.txt") as introduction_last_id_file:
  introduction_last_id = introduction_last_id_file.read().strip()

# Get the timeline items since the last one that was checked
timeline = Mastodon.timeline_hashtag(
  mastodon,
  'introduction',
  local=True,
  since_id = int(introduction_last_id)
)

# If there's posts to check
if len(timeline) > 0:
  os.remove("introduction_last_id.txt") # Delete the local file that
                                        # contains the ID of the last
                                        # post that was checked

  # Then make a new file that contains the ID of the new latest post
  # that was checked
  with open("introduction_last_id.txt", "a") as introduction_last_id_file:
    introduction_last_id_file.write(str(timeline[0].get("id")))

# Loop through them all and boost them
for toot in timeline:
  # with open("log/" + str(toot.get("id")) + ".txt", "a") as logfile:
    # logfile.write(toot.get("url") + "\n")
    # logfile.write(toot.get("content"))
  mastodon.status_reblog(toot.get("id"))

# Now do it all again for "Introductions"

with open("introductions_last_id.txt") as introductions_last_id_file:
  introductions_last_id = introductions_last_id_file.read().strip()

timeline = Mastodon.timeline_hashtag(
  mastodon,
  'introductions',
  local=True,
  since_id = int(introductions_last_id)
)

if len(timeline) > 0:

  os.remove("introductions_last_id.txt")

  with open("introductions_last_id.txt", "a") as introductions_last_id_file:
    introductions_last_id_file.write(str(timeline[0].get("id")))

for toot in timeline:
  # with open("log/" + str(toot.get("id")) + ".txt", "a") as logfile:
    # logfile.write(toot.get("url") + "\n")
    # logfile.write(toot.get("content"))
  mastodon.status_reblog(toot.get("id"))
