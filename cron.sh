#!/bin/bash

cd /path/to/socrates_bots/
source env/bin/activate
python3 socrates_introductions.py
python3 socrates_image_descriptions.py
python3 socrates_crossposters.py
