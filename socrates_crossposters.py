## I recommend running this script using the folder in which the
## script is contained as the working directory, so that the user
## credentials and the log folder (if you enable that) are all in the
## same place
from mastodon import Mastodon

import os

mastodon = Mastodon(
    access_token = 'pytooter_usercred.secret',
    api_base_url = 'https://scholar.social' # This will need to be
                                            # updated with the correct
                                            # instance
)

# Get the ID of the last post that was checked
with open("crossposters_last_id.txt") as crossposters_last_id_file:
  crossposters_last_id = crossposters_last_id_file.read().strip()

# Get the timeline items since the last one that was checked
timeline = Mastodon.timeline_local(
  mastodon,
  since_id = int(crossposters_last_id)
)

# If there's posts to check
if len(timeline) > 0:
  os.remove("crossposters_last_id.txt") # Delete the local file that
                                        # contains the ID of the last
                                        # post that was checked
  
  # Then make a new file that contains the ID of the new latest post
  # that was checked
  with open("crossposters_last_id.txt", "a") as crossposters_last_id_file:
    crossposters_last_id_file.write(str(timeline[0].get("id")))

# Loop through the posts retrieved above and if the name of the app
# that posted it is in the conditional below, send them a message

# Uncomment the stuff below if you want some feedback for debugging
# written to a log file
for toot in timeline:
  # with open("log/" + str(toot.get("id")) + ".txt", "a") as logfile:
    # logfile.write(toot.get("url") + "\n")
    # logfile.write(toot.get("content") + "\n")
    if "application" in toot:
      tootapp = toot.get("application")
      # logfile.write(tootapp.get("name"))
      if tootapp.get("name") == "Mastodon Twitter Crossposter" or tootapp.get("name") == "Moa":
        mastodon.status_post(
          "@" + toot.get("account")["acct"] + " Hi, just a reminder that automatically cross-posting from Twitter is not allowed on this instance on the Public privacy setting\n\nPlease deactivate your Twitter cross-poster or your account may be limited\n\nThanks for understanding!",
          in_reply_to_id = toot.get("id"),
          visibility = "direct"
        )
