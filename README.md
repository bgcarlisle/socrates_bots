# socrates_bots

Bots for the [@socrates](https://scholar.social/@socrates) account on
scholar.social

# Setup

1. Make a Python virtual environment in your `socrates_bots/`
directory
2. Install `Mastodon.py` there
   ([docs](https://mastodonpy.readthedocs.io/en/stable/))
3. Use `registerapp.py` to register the Socrates Introductions Bot on
   the server (update the base URL first; this will produce a file
   named `pytooter_clientcred.secret`)
4. Use `login.py` to log in as the user that will be boosting
   introductions (update the base URL, username and password; this
   will produce a file named `pytooter_usercred.secret` that will be
   used by all the other Python scripts)
5. Update the base URLs in `socrates_introductions.py`,
   `socrates_image_descriptions.py`, and `socrates_crossposters.py`
6. Update the path to the bot script, the path to your Python virtual
   environment in the `cron.sh` script and make `cron.sh` executable
7. Add the following line to your `crontab` (update it so the path is
   correct):

```
* * * * * /path/to/socrates_bots/cron.sh
```

This should run the script every minute
