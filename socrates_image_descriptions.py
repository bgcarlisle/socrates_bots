## I recommend running this script using the folder in which the
## script is contained as the working directory, so that the user
## credentials and the log folder (if you enable that) are all in the
## same place
from mastodon import Mastodon

import os

mastodon = Mastodon(
  access_token = 'pytooter_usercred.secret',
  api_base_url = 'https://scholar.social' # This will need to be
                                          # updated with the correct
                                          # instance
)

# Get the ID of the last post that was checked
with open("imagedesc_last_id.txt") as imagedesc_last_id_file:
  imagedesc_last_id = imagedesc_last_id_file.read().strip()

# Get the timeline items since the last one that was checked
timeline = Mastodon.timeline_local(
  mastodon,
  since_id = int(imagedesc_last_id)
)

# Some feedback for debugging
print("Last ID checked: " + str(imagedesc_last_id))

print("Checking " + str(len(timeline)) + " posts on the local timeline")

# If there's posts to check
if len(timeline) > 0:
  os.remove("imagedesc_last_id.txt") # Delete the local file that
                                     # contains the ID of the last
                                     # post that was checked

  # Then make a new file that contains the ID of the new latest post
  # that was checked
  with open("imagedesc_last_id.txt", "a") as imagedesc_last_id_file:
    imagedesc_last_id_file.write(str(timeline[0].get("id")))

# Empty array of messages
sentmessages = []

# Loop through the posts retrieved above and if there is media without
# a description and that post hasn't been replied to already, send
# them a message

# Uncomment the stuff below if you want some feedback for debugging
# written to a log file
for toot in timeline:
    # with open("log/" + str(toot.get("id")) + ".txt", "a") as logfile:
        # logfile.write(toot.get("url") + "\n")
        # logfile.write(toot.get("content") + "\n")    
        # logfile.write(str(toot.get("media_attachments")))
    for media in toot.get("media_attachments"):
        if media["description"] == "" or media["description"] == None:
            print("Warning: " + str(toot.get("id")))
            if not toot.get("id") in sentmessages:
                mastodon.status_post(
                    "@" + toot.get("account")["acct"] + " Hi, just a reminder that it is an expectation of this instance that images posted using the Public privacy setting have accurate image descriptions to aid in accessibility\n\nPlease edit this post to add this using the web app image description tool\n\nThanks for understanding!",
                    in_reply_to_id = toot.get("id"),
                    visibility = "direct"
                )
            sentmessages.append(toot.get("id"))
